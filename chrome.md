# Google Chrome

## Extensions

- ublock
- umatrix
- password alert - google
- instapaper
- chrome remote desktop
- data saver - google
- tampermonkey
- edit this cookie
- TeX the world
- react developer tools
- whatfont
- octotree
- enhanced steam
- amateur voat enhancements

- the great suspender
- 45to75

- google cast
- earth view from google maps
- project naptha
- reedy
- hemingway
- cryptocat
- oddshot
- SHINE for reddit

## sites

- facebook
- twitter
- gmail
- reddit
- steam
- youtube
- twitch
- github
- bitbucket
- codepen
- waffle.io
- guildbit
- maildrop
- protonmail
