# Games

## Platforms
- Steam
- dolphin
- visual boy advanced - m
- emu-os
- GoG Galaxy
- Mod Organizer

## Strategy
- Civilization
- Crusader Kings II
- Europa Universalis IV
- Stellaris
- Cities Skylines
- Patrician III
- openttd
- openrct
- openage

## Tactics
- Planetary Annihilation
- Age of Wonders III
- Total War Warhammer
- Xenonauts
- XCOM
- Door Kickers
- King's Bounty
- Homeworld Remastered
- interplanetary
- executive assault
- oriental empires
- openxcom

## RPG
- Elder Scrolls  
- GTA
- Mount & Blade
- Borderlands
- Witcher III
- Just Cause
- Assassins's Creed
- Dark Souls
- Deus Ex
- Fallout
- Sleeping Dogs
- Divinity Original Sin
- Star Citizen
- openmw

## FPS
- CSGO
- TF2
- Insurgency
- Arma 3
- Payday
- Ground Branch
- Rainbow Six Siege

## Stealth
- Hitman
- the masterplan
- prison architect
- gunpoint
- volume

## Driving
- Assetto Corsa
- Grid 2
- Euro Truck Simulator
- Farming Simulator
- Dirt 3

## Paradox
- Besiege
- Kerbal Space Program
- Portal
- SpaceChem
- infinifactory
- Brothers A Tale of Two Sons
- Space/Medieval Engineers
- Mini Metro
- poly bridge
- home improvisation
- minetest
- powder toy
- minecraft

## Action
- Dota 2
- Shovel Knight
- Garry's Mod
- Magicka
- Trine
- Star Wars Battlefront
- Octodad Dadliest Catch
- Gauntlet
- party hard
- keep talking and nobody explodes
- battleblock theater

## Simulation
- Football Manager
- Papers Please
- Town of Salem
- Tabletop Simulator
- shoppe keep
