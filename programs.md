# Programs

## Code
- git
- atom

- p4merge
- IntelliJ

### languages
- node
- python

## Communication
- Chrome
- open broadcaster
- Mumble
- Murmur

- Lynx
- GoogleCL
- n1 (email client)
- Signal Secure Messenger
- Tox
- mattermost
- waffle.io
- voicemeeter
- mutt (email)
- rtv (reddit)
- irssi
- Tryton

## Media
- mpd with ncmpcpp
- vlc
- Steam
- Wine

- foobar2000
- cmus
- DeaDBeeF
- mixxx
- Kodi (home theater)

## Creation
- GIMP
- Inkscape
- Krita
- darktable
- Hugin
- Blender
- yafaray
- synfig
- antimony cad
- KiCad
- Unity
- ardour
- Audacity
- LibreOffice
- Microsoft Office
- homesweethome3d
- texlive

- matplotlib
- paraview

## Utility
- htop
- transmission-remote-cli
- p7zip
- archey3

- gittorrent
- syncthing
- rTorrent with ruTorrent
- unigen valley, heaven
- cinebench
- dispcalGUI
- Speccy
- ccleaner
- windirstat
- iograph
- whatpulse
- unlocker
- malwarebytes
- units (command line tool)
- rtop
- sysstat
- glances
- screenfetch
- conky
- arch-wiki-lite

## Robotics
- home assistant

## Networking
- nmap
- sshuttle
- netcat
- ngrep
- siege
- mitmproxy
- curl
- wget
- find
- rsync

## linux system
- arch
- btrfs
- i3 (gaps)
- urxvt
- zsh

- i3bar
- wmutils
- py3status
- lemonboy bar
- tmux
- ARandR
- x2go
- fish shell
- vim powerline
- vundle.vim
- vim-airline
- syntastic
- vim bootstrap
- neovim
- vim-sensible
- vim config

## easter egg
- freesweep
