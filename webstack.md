# web stack

## server
- saltstack
- centos

## software stack
- openstack
- coreos
- kubernetes
- cassandra
- redis
- elastic search
- haproxy
- nginx

## software alternatives
- google compute engine api openstack

## application
- flask

## application tooling
- Jenkins
- Phabricator

## frontend
- reactjs
- redux

#### frontend tooling
- babel
- flow
- gulp

