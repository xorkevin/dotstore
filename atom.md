# Atom packages

## tools
- color-picker
- minimap
- atom-beautify
- merge-conflicts
- project-manager
- lorem-ipsum
- turbo-javascript
- pigments
- minimap-pigments
- vim-mode
- editor-stats
- markdown preview
- atom-alignment
- change-case
- multi-cursor
- pdf-view
- kite

## language specific
- atom-ternjs
- angularjs
- atom-bootstrap3
- react
- language-rust
- racer
- linter
- linter-eslint
- linter-rust
- linter-flake8
- LaTeX
- latexer
- language-latex
- autocomplete-python
- go-plus

## ui
- material ui/ syntax dark
- file-icons

## easter egg
- activate-power-mode
- achievements
- asteroids

